const CONFIG = require('./config.json');
//const express = require('express');
//const app = express();
//const server = require('http').Server(app);
//const io = require('socket.io')(server);

//const socketClients = require('./controllers/client_handler')(io);

//app.use(express.static('public'));

//server.listen(3000, () => console.log('Game running'));



//handle to the WebSockets Module
const ws = require("ws");

//Start a new WebSocketServer
const wss = new ws.WebSocketServer({ port: 3000 });

const socketClients = require('./controllers/client_handler')(wss, ws);

/*
wss.on('connection', function connection(ws) {
  ws.on('message', function message(data) {
    console.log('received: %s', data);
  });

  ws.send('something');
});
*/
