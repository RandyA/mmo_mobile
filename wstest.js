//handle to the WebSockets Module
const ws = require("ws");

//Start a new WebSocketServer
const wss = new ws.WebSocketServer({ port: 3000 });


wss.on('connection', function connection(ws) {
  ws.on('message', function message(data) {
    console.log('received: %s', data);
  });

  ws.send('something');
});

//console.log(wss);