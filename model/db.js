/*
handles the DB Connection and has all the methods for DB interaction
*/

'use strict';

//Mysql Implementation
var mysql = require('mysql2/promise');

//Class based implementation
class MySQLHandler {

/*
This is th model object. All data retrieval functions including 
randomization events should happen here with params passed from the
controller

All functions that respond will need 'res' to be passed
*/

  constructor(){
    console.log("MySQL_Handler: Built DB Object");
    this.host = "localhost";
    this.user = "root";
    this.pass = "root";
    this.database = "mmo";
  }

  //Returns all items By Class ID
  async getUser(user_id){

    console.log("MySQL_Handler: Fetching Items By Class");

    const con = await mysql.createConnection({
      host: this.host,
      user: this.user,
      password: this.pass,
      database: this.database
    });
    
    var sql = "SELECT * FROM user WHERE id = ?";

    const [rows, fields] = await con.execute(sql, [user_id]);
    
    //console.log(rows[0].id);
    
    return rows;
  }

  //Pick an item and Save it to player, TODO: Expand this to full item details (join other tables)
  async save(user_id){
    console.log("MySQL_Handler: Fetching Item and Saving");
    const con = await mysql.createConnection({
      host: this.host,
      user: this.user,
      password: this.pass,
      database: this.database
    });

    //Grab the Item info for return
    let sql = "SELECT * FROM item WHERE id = ?";
    const [rows, fields] = await con.execute(sql, [item_id]);

    //Confirm item exists before saving it to player
    if (rows[0].id == item_id){
      
      //Save the item to a player
      let sql = "INSERT INTO user ('user_id', 'item_id') VALUES (?, ?)";
      const [ins_rows, ins_fields] = await con.execute(sql, [ user_id, item_id ]);

      //Return the info of item selected
      return rows[0];

    } else {
      //Item not found
      return {"error" : "Item not found"};
    }
  }
}

//Export the class to the module object
module.exports = new MySQLHandler();