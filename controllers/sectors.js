const gameSettings = require('./game_settings');

const sectors = [];

module.exports = {

    getRandomColor() {
        const color = [0, 0, 0];
        for (let i = 0; i < 3; i += 1) {
            color[i] = Math.floor(Math.random() * (256 - 0)) + 0;
        }
        return color;
    },
    getRandomPosition() {
        
        const x = Math.floor(Math.random() * (gameSettings.universeX - (-1 * gameSettings.universeX)) + (-1 * gameSettings.universeX));
        const y = Math.floor(Math.random() * (gameSettings.universeY - (-1 * gameSettings.universeY)) + (-1 * gameSettings.universeY));
        const z = Math.floor(Math.random() * (gameSettings.universeZ - (-1 * gameSettings.universeZ)) + (-1 * gameSettings.universeZ));
        
        //const x = Math.floor(Math.random() * gameSettings.universeX);
        //const y = Math.floor(Math.random() * gameSettings.universeY);
        //const z = Math.floor(Math.random() * gameSettings.universeZ);
        return [x, y, z];
    },
    getRandomSize(){
        return Math.floor(Math.random() * 15) + 5;
    },
    setupSectors(){
        
        let numberOfSectors = 3;

        for (let x = 0; x < numberOfSectors; x++){

            let coordinates = this.getRandomPosition();
            let rgbArray = this.getRandomColor();
            let sectorSize = this.getRandomSize();

            sectors.push({
                id: "Sector_" + x,
                //width: gameSettings.playerWidth,
                //height: gameSettings.playerHeight,
                x: coordinates[0],
                y: coordinates[1],
                z: coordinates[2],
                //color: `rgba(${rgbArray[0]},${rgbArray[1]},${rgbArray[2]},1)`,
                color: rgbArray,
                size: sectorSize,
            });
        }
    },
    getSectorById(id) {
        let sector;
        sectors.forEach(((element, index, array) => {
            if (element.id == id) {
                sector = array[index];
            }
        }));
        return sector;
    },
    exisitingSector(id) {
        let existing = false;
        sectors.forEach(((element) => {
            if (element.id == id) {
                existing = true;
            }
        }));
        return existing;
    },
    getAllSectors() {
        return sectors;
    },
};