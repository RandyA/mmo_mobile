const gameSettings = require('./game_settings');

//TODO: Create Player Class
const players = [];

function lerp (start, end, amt){
    return (1-amt)*start+amt*end
}

function directValue(start, end, amt){
    if (start > end){
        start -= amt;
    } else if (start < end){
        start += amt;
    }
    return (Math.round(start * 100) / 100);
}

module.exports = {

    getRandomColor() {
        const color = [0, 0, 0];
        for (let i = 0; i < 3; i += 1) {
            color[i] = Math.floor(Math.random() * (256 - 0)) + 0;
        }
        return color;
    },

    getRandomPosition() {
        //const x = Math.floor(Math.random() * (gameSettings.gameWidth - gameSettings.playerWidth)) + gameSettings.playerWidth;
        //const y = Math.floor(Math.random() * (gameSettings.gameHeight - gameSettings.playerHeight)) + gameSettings.playerHeight;
        const x = Math.floor(Math.random() * (gameSettings.universeX - (-1 * gameSettings.universeX)) + (-1 * gameSettings.universeX));
        const y = Math.floor(Math.random() * (gameSettings.universeY - (-1 * gameSettings.universeY)) + (-1 * gameSettings.universeY));
        const z = Math.floor(Math.random() * (gameSettings.universeZ - (-1 * gameSettings.universeZ)) + (-1 * gameSettings.universeZ));
        
        //const x = Math.floor(Math.random() * gameSettings.universeX);
        //const y = Math.floor(Math.random() * gameSettings.universeY);
        //const z = Math.floor(Math.random() * gameSettings.universeZ);
        return [x, y, z];
    },

    addPlayer(id, displayname) {
        const coordinates = this.getRandomPosition();
        const rgbArray = this.getRandomColor();
        if (!this.exisitingPlayer(id)) {
            players.push({
                id,
                //width: gameSettings.playerWidth,
                //height: gameSettings.playerHeight,
                x: coordinates[0],
                y: coordinates[1],
                z: coordinates[2],
                //color: `rgba(${rgbArray[0]},${rgbArray[1]},${rgbArray[2]},1)`,
                color: rgbArray,
                speed: gameSettings.speed,
                requested_position: undefined,
                displayname: displayname,
            });
        }
    },

    isAtPoint(id){
        let iap = false;
        const player = this.getPlayerById(id);
        if (player.x == player.requested_position.x && 
            player.y == player.requested_position.y &&
            player.z == player.requested_position.z) {
                iap = true;
        }
        return iap;
    },

    moveToPoint(id, position){
        console.log("moving to point");
        const player = this.getPlayerById(id);
        player.requested_position = position;

        console.log("X: " + player.x + " " + position.x);
        console.log("Y: " + player.y + " " + position.y);
        console.log("Z: " + player.z + " " + position.z);

        if (player) {
            player.x = directValue(player.x, position.x, player.speed);
            player.y = directValue(player.y, position.y, player.speed);
            player.z = directValue(player.z, position.z, player.speed);
        }
    },

    move(id, direction) {
        const player = this.getPlayerById(id);
        if (player) {
            switch (direction) {
                case 0:
                    if ((player.x + 20) < (gameSettings.gameWidth - (gameSettings.playerWidth / 2))) { player.x += 20; }
                    break;
                case 90:
                    if ((player.y - 20) > 0) { player.y -= 20; }
                    break;
                case 180:
                    if ((player.x - 20) > 0) { player.x -= 20; }
                    break;
                case 270:
                    if ((player.y + 20) < (gameSettings.gameHeight - (gameSettings.playerWidth / 2))) { player.y += 20; }
                    break;
                default:
                    console.log('no action');
            }
        }
    },

    getPlayerById(id) {
        let player;
        players.forEach(((element, index, array) => {
            if (element.id == id) {
                player = array[index];
            }
        }));
        return player;
    },

    exisitingPlayer(id) {
        let existing = false;
        players.forEach(((element) => {
            if (element.id == id) {
                existing = true;
            }
        }));
        return existing;
    },

    getAllplayers() {
        return players;
    },

    removePlayer(id) {
        let index = -1;
        for (let i = 0; i < players.length; i++) {
            if (players[i].id == id) {
                index = i;
            }
        }
        players.splice(index, 1);
    },
};