const playerController = require('./player');
const gameSettings = require('./game_settings');
const uuid = require('uuid');


//Note: This was renamed from planetController (may be lingering references to planets)
const sectorController = require('./sectors');
sectorController.setupSectors();

/* Working Simple Example 
wss.on('connection', function connection(ws) {
  ws.on('message', function message(data) {
    console.log('received: %s', data);
  });

  ws.send('something');
});*/

module.exports = function (wss, ws) {
    setInterval(() => {

        //Check Player Movement
        let players = playerController.getAllplayers();

        /*
        players.forEach( (p) => {
            if ( typeof p.requested_position !== "undefined"){
                //console.log("requested position found");
                //console.log(p.requested_position);

                if ( !playerController.isAtPoint(p.id) ){
                    playerController.moveToPoint(p.id, p.requested_position);
                }
            }
        });*/

        
        //io.sockets.emit('gamedata', playerController.getAllplayers());
        wss.clients.forEach(function each(client) {
            if (client !== ws && client.readyState === ws.OPEN) {
                //client.send(playerController.getAllplayers());
            }
        });

    }, gameSettings.fps);

    //TODO: Fully form this to replace the io version below
    wss.on('connection', function connection(ws, req) {

        //Send first Message
        ws.send( JSON.stringify({status : "system_message", payload: ["connected to server"]}) );

        //Receiving Messages from Client
        ws.on('message', function message( data ) {

            //ALL DATA RECEIVED: should follow a String action, Array payload structure 
            data = JSON.parse(data);
            console.log('received: %s', data);


        
            //Handle New Player Added //TODO: Repeat for all Requested Actions (eg. request_move, request_chat, etc)
            if (data.requested_action == "init_player"){

                ws.id = uuid.v4();
                
                //This replaces the unique ID of the socket.id
                let player_name = data.payload[0];
                let socket_id = ws.id;//req.session.userId;

                if (!player_name.length) {
                    player_name = socket_id;
                }

                ws.player_name = player_name;

                console.log(`New connection ${socket_id} - ${player_name}`);

                //Add the new player to the playerController object
                playerController.addPlayer(socket_id, player_name);

                //Return success status messages back to client
                ws.send( JSON.stringify({status : "system_message", payload: ["Player Initialized", "receiving connection id"]}) );
                ws.send( JSON.stringify({status : "connection_id", payload: [socket_id]}) );
                ws.send( JSON.stringify({status : "system_message", payload: ["Total number of players in game: " + playerController.getAllplayers().length]}) );

                console.log("Added new player, all current players:");
                console.log( playerController.getAllplayers() );
                console.log("Total number of players in game: " + playerController.getAllplayers().length );

                //Send the new player details to all players
                wss.clients.forEach(function each(client) {
                    if (client !== ws && client.readyState === ws.OPEN) {
                        //Send the new player count to all players
                        client.send( JSON.stringify({status: "player_count", payload: [playerController.getAllplayers().length]}) );
                    }
                });
                
            } else if (data.requested_action == "request_chat") {

                //Send the chat contents to all players
                wss.clients.forEach(function each(client) {
                    if (client.readyState === ws.OPEN) {
                        //Send the chat with the players name along with contents
                        client.send( JSON.stringify({status : "new_chat", payload: [ws.player_name, data.payload[0]]}) );
                    }
                });
                console.log("Chat from: '" + ws.player_name + "' sent to all players.");
                console.log("Chat message: " + data.payload);

            } else if (data.requested_action == "request_nearby_players"){
                //Get Nearby Players 

                let player_id = data.payload[0];

                //Iterate through all and find matches
                let nearbyPlayers = playerController.getNearbyPlayersFrom(player_id);

                //Send all matches back to requester
                ws.send( JSON.stringify({status : "sector_players", payload: [nearbyPlayers]}) );
            }
        
            
        });

        ws.on('close', function close() {
            console.log('client disconnected');
            playerController.removePlayer(ws.id);

            //Send the removal to the client
            wss.clients.forEach(function each(client) {
                if (client !== ws && client.readyState === ws.OPEN) {
                    //Send the ID of the player that left
                    client.send( JSON.stringify({status: "remove_player", payload: [ws.id]}) );
                }
            });
        });

    });

    /*
    io.on('connection', (socket) => {
        
        //This replaces the unique ID of the socket.id
        let player_name = socket.handshake.query.name;
        if (!player_name.length){
            player_name = socket.id;
        }

        console.log(`New connection ${socket.id} - ${player_name}`);
        socket.emit(
            'create',
            {
                //canvasWidth: gameSettings.gameWidth,
                //canvasHeight: gameSettings.gameHeight,
                id: socket.id,
                displayname: player_name,
                sectors: sectorController.getAllSectors()
            }
        );
        
        playerController.addPlayer(socket.id, player_name);
        
        socket.on('requestmove', (direction) => {
            playerController.move(socket.id, direction);
        });

        socket.on('requestmovetopoint', (position) => {
            playerController.moveToPoint(socket.id, position);
        });

        //On Chat Submission, send to all players
        socket.on('requestchat', (message) => {
            io.sockets.emit('newChat', player_name + ": " +message);
        });

        //On Disconnection by a player
        socket.on('disconnect', () => {
            console.log('Client left');
            playerController.removePlayer(socket.id);

            //Send the removal to the client
            io.sockets.emit('removePlayer', socket.id);
        });
    }); */
};

console.log("Client Handler Loaded");